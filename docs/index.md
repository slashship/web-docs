# Welcome to Slash Ship

## Needed to quote

* From address (at a minimum: street, zip code, country)
* To address (at a minimum: street, zip code, country)
* Item details: we can include flat rate services in our suggestions, or...
* or if packed: Parcel details (length, width, height in inches, weight in ounces)
* Insurance value of each item (if more than carrier standard insurance is wanted)

If international:

* HS code
* Type of shipment (sample, merchandise)
* Who will pay any duties or fees (recipient, sender)
* Customs value of each item


## Things to be aware of

### Duty / HS Code

When shipping internationally, more information is needed for each item in the shipment. One of those things is the HS Code, and one thing it is used for is to apply duties and taxes.

Some resources for figuring out the correct code:
* https://hts.usitc.gov/
* https://www.flexport.com/data/hs-code/0-all-commodities
* http://www.faqs.org/rulings/tariff.html



### About us

We save you up to 48% on shipping and make it easy so you can focus on growing your business.




---
© 2019 Eric Norman and Mark Wachtler
