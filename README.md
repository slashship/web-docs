# Markdown reference
[Markdown Guide / Cheat Sheet](https://www.markdownguide.org/cheat-sheet)

# Venv setup

Install Homebrew, use the curl on their website

Install python with home-brew
`brew install python`

Install virtual env
`pip3 install virtualenv`

https://virtualenv.pypa.io/en/latest/installation/


If needed, create a virtual environment with python 3 only:

`virtualenv -p python3 venv`
[https://stackoverflow.com/questions/23842713/using-python-3-in-virtualenv]


Switch to that venv...
`source venv/bin/activate`


...and install packages from requirements.txt:

`$ pip install -r requirements.txt`

To confirm venv was setup successfully, enter `pip list`.
The response should include mkdocs and this or a newer version:
```
Package    Version
---------- -------
mkdocs     1.0.4  
pip        19.1.1 
```

For `python -V`, the response should be a version number >=3.71, as in `Python 3.7.1`

# Run demo server

`$ mkdocs serve`

then open the IP address in a browser.


# Deploy

Have webhook integration, so pushing to the repo should trigger a build which will go live.

